# Sedona
HTML Academy HTML&amp;CSS Intensive Course training project

Hand-coded from the .psd file provided by [HTML Academy](https://htmlacademy.ru/)

## See it in action
**[Click.](https://htmlpreview.github.io/?https://raw.githubusercontent.com/Nick-Shmyrev/sedona/master/index.html)**
